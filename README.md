# 1 Front-End Kubernetes cluster, 1 Back-End cluster
The intention of this project is to practice setting up a microservice architecture for multiple microservices.

## Current working Architecture:
Frontend cluster instance sends request to backend ingress controller.

## Backend
#### Microservice1 pod (Django Rest + PostgreSQL)
Any of the multiple REST API containers searches for the data in its local SQL DB, if not found, then fetch from distant API. After fetch send new data to cluster DB
#### Microservice2 pod (Django Rest + REDIS cache)
This microservice is an authentication server. It will have a Redis Cache to store the Tokens and their validity. And connect to the clusterDB to save new username and password, and to login when token expires.
#### Microservice3 pod (Golang (Gin + Gorm) self made MVC Framework + PostgreSQL)
This microservice will be a simple API to help develop an MVC Framework to work with Go and develop mantainable APIs

#### Cluster DB (NextJs(NodeJs) + MongoDB)
It recieves any data structure and stores it in its NoSQL instance. Its the central repository for all possible future microservices (or just one instance of it for each microservice to mantain atomicity)