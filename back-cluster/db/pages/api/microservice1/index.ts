export default (req: any, res: any) => {
  res.statusCode = 404;
  const response = {
    success: "false",
    code: 404,
    error: "No match found",
  };
  res.json(response);
};
