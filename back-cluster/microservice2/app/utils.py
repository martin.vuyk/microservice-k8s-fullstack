import datetime
from app.models import User
import rsa


def valid_credentials(user: User, token: str) -> bool:
    if datetime.datetime.now() < user.valid_until:
        return get_token(user) == token
    return False


def get_token(user: User) -> str:
    return rsa.encrypt(
        f"{user.username}:{user.hashed_password}".encode("ascii"), user.temp_key
    )

def add_user_to_cache(username, hashed_password, temp_key, valid_until):
    if User.is_stored_in_DB(username):
        user = User.objects.get(username=username)
        user.hashed_password = hashed_password
        user.temp_key = temp_key
        user.valid_until = valid_until
        user.save()
    else:
        User.objects.create(
            username=username,
            hashed_password=hashed_password,
            temp_key=temp_key,
            valid_until=valid_until,
        )
    return User.objects.get(username=username)
