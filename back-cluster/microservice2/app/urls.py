from .views import *
from django.urls import path

urlpatterns = [
    path("validate_token", ValidateToken.as_view(), name="validate_token"),
]
