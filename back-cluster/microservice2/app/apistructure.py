from django.db import models
import abc
from typing import List
from rest_framework.views import APIView
from rest_framework.response import Response


class APIModelStructure(models.Model):
    @abc.abstractmethod
    @staticmethod
    def is_stored_in_DB(identifier) -> bool:
        pass

    @abc.abstractmethod
    def get_object_data() -> dict:
        pass

    @staticmethod
    def get_objects_data(objects: List["APIModelStructure"]):
        return [object.get_object_data() for object in objects]


class APIViewStructure(APIView, abc.ABC):
    @abc.abstractmethod
    def post() -> Response:
        pass

    @abc.abstractmethod
    def get() -> Response:
        pass
