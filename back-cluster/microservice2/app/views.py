import json
import requests
from rest_framework.response import Response
from app.apistructure import APIViewStructure
from app.models import User
from app.utils import valid_credentials, get_token, add_user_to_cache


class ValidateToken(APIViewStructure):
    def post(self, request, *args, **kwargs):
        return Response(json.dumps({"error": "Endpoint not valid"}, status=404))

    def get(self, request, *args, **kwargs):
        try:
            try:
                username = request.username
                token = request.token
                is_registered = User.is_stored_in_DB(username)
                if not is_registered:
                    raise Exception("user not registered", 404)
                user = User.objects.get(username=username)
                if not valid_credentials(user, token):
                    raise Exception("invalid credentials", 401)
                response = dict(success=True, code=200, message="valid cretentials")
                return Response(json.dumps(response), status=200)
            except Exception as e:
                if len(e.args) != 2:
                    raise Exception(str(e), 500)
                raise Exception(e)
        except Exception as e:
            response = dict(success=False, code=e.args[1], error=e.args[0])
            return Response(json.dumps(response), status=e.args[1])


class Register(APIViewStructure):
    def post(self, request, *args, **kwargs):
        try:
            try:
                username = request.username
                password = request.password
                clusterdb_address = "https://url"
                is_registered_in_cache = User.is_stored_in_DB(username)
                is_registered_in_clusterdb = requests.get(clusterdb_address)
                if is_registered_in_cache or is_registered_in_clusterdb:
                    raise Exception("username already exists", 400)
                register_in_clusterdb = requests.post(
                    clusterdb_address,
                    data=json.dumps(dict(username=username, password=password)),
                )
                db_response = json.loads(register_in_clusterdb.text())
                if not db_response.success:
                    raise Exception(
                        f"failed to register: {db_response.error}", db_response.code
                    )
                response = dict(
                    success=True, code=200, message="registered successfully"
                )
                return Response(json.dumps(response), status=200)
            except Exception as e:
                if len(e.args) != 2:
                    raise Exception(str(e), 500)
                raise Exception(e)
        except Exception as e:
            response = dict(success=False, code=e.args[1], error=e.args[0])
            return Response(json.dumps(response), status=e.args[1])

    def get(self, request, *args, **kwargs):
        return Response(json.dumps({"error": "Endpoint not valid"}, status=404))


class Login(APIViewStructure):
    def post(self, request, *args, **kwargs):
        try:
            try:
                username = request.username
                password = request.password
                clusterdb_address = "https://url"
                get_user_clusterdb = requests.get(
                    clusterdb_address,
                    data=json.dumps(dict(username=username, password=password)),
                )
                db_response = json.loads(get_user_clusterdb.text())
                if not db_response.success:
                    raise Exception("username not registered", 404)
                user = add_user_to_cache(username, db_response.data.hashed_password, 
                                             db_response.data.temp_key, db_response.data.valid_until)
                token = get_token(user)
                response = dict(success=True, code=200, data=token)
                return Response(json.dumps(response), status=200)
            except Exception as e:
                if len(e.args) != 2:
                    raise Exception(str(e), 500)
                raise Exception(e)
        except Exception as e:
            response = dict(success=False, code=e.args[1], error=e.args[0])
            return Response(json.dumps(response), status=e.args[1])

    def get(self, request, *args, **kwargs):
        return Response(json.dumps({"error": "Endpoint not valid"}, status=404))