from django.db import models
from app.apistructure import APIModelStructure


class User(APIModelStructure):
    username = models.CharField(max_length=255, unique=True, null=False, blank=False)
    hashed_password = models.CharField(max_length=255, unique=True, null=False, blank=False)
    temp_key = models.CharField(max_length=255, unique=True, null=False, blank=False)
    valid_until = models.DateTimeField()

    def get_object_data(self):
        return {
            "username": self.username,
            "hashed_password": self.hashed_password,
            "temp_key": self.temp_key,
            "valid_until": self.valid_until
        }

    @staticmethod
    def is_stored_in_DB(identifier):
        try:
            query_result = User.objects.get(username=identifier)
            if query_result:
                return True
            return False
        except:
            return False
