## Microservice 2 - Authentication Server

This microservice is an authentication server. It will have a Redis Cache to store the Tokens and their validity. And connect to the clusterDB to save new username and password, and to login when token expires.

### Endpoints:

localhost:4595/admin	-> Django's admin page<br />
localhost:4595/	        -> <br />

### Deployment:
beautifully simple:

-download the folder<br />
-put a .env file inside with the following fields: <br />
SQL_ENGINE= still working on joining redis to django<br />
SQL_DATABASE=<br />
SQL_USER=<br />
SQL_PASSWORD=<br />
SQL_HOST=db<br />
SQL_PORT=6379<br />

-in a terminal, write:  "docker compose up"

#### Requirements:
-docker