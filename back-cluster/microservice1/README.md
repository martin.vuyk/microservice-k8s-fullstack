## Microservice 1 - Pokemon data store

This microservice is a simple use case where I apply a simple Framework I built to better the readability and documentation for a Django Rest API, and add seamless dev deployment with Docker-compose

### Endpoints:

localhost:4595/admin -> Django's admin page<br />
localhost:4595/api/v1/'id' -> get pokemon with said id (1 up to 100 000)<br />

for more comprehensive documentation:<br />
localhost:4595/api/v1/docs/swagger -> swagger docs for API<br />
localhost:4595/api/v1/docs/redoc -> redoc docs for API<br />
download folder and execute ./scripts/build_dev_env && ./scripts/generate_docs -> generates pdoc3 html docs for the whole python package under ./docs<br />

### Deployment:

-download the folder<br />
-put a .env file inside with the following fields:<br />
SQL_ENGINE=django.db.backends.postgresql<br />
SQL_DATABASE=<br />
SQL_USER=<br />
SQL_PASSWORD=<br />
SQL_HOST=db<br />
SQL_PORT=5432<br />

-in a terminal, write: "docker compose up"

#### Requirements:

-docker
