from django.db import models
from app.data_models.apistructure import APIModelStructure


class Pokemon(APIModelStructure, models.Model):
    uid = models.CharField(max_length=255, unique=True, null=False, blank=False)
    data = models.JSONField(unique=True, null=False, blank=False)

    def get_object_data(self):
        return {
            "uid": self.uid,
            "data": self.data,
        }

    @staticmethod
    def is_stored_in_DB(identifier):
        try:
            query_result = Pokemon.objects.get(uid=identifier)
            if query_result:
                return True
            return False
        except:
            return False
