from django.contrib import admin
from app.models import Pokemon

# Register your models here.
admin.site.register(Pokemon)
