"""
This file is responsible for mantaining structure and reducing code redundancy in the API.
"""
from dataclasses import dataclass, fields
import json
from django.db import models
from abc import abstractmethod
from typing import Any, Callable, Iterator, Dict, Literal, Optional, Union
from rest_framework.response import Response
from rest_framework.views import APIView


class APIModelStructure:
    """Structure of Models in the DB"""

    @classmethod
    def is_stored_in_DB(cls: models.Model, field: str, value: Any) -> bool:
        return cls.objects.filter(**{field: value}).exists()

    @abstractmethod
    def get_object_data() -> Dict:
        pass

    @staticmethod
    def get_objects_data(objects: Iterator["APIModelStructure"]):
        return [object.get_object_data() for object in objects]


@dataclass
class ApiResponse:
    """The format of the responses from API Views"""

    success: Union[Literal["true"], Literal["false"]]
    code: int
    data: Optional[Any] = None
    error: Optional[str] = None

    def __dict__(self):
        return {field.name: str(getattr(self, field.name)) for field in fields(self)}


class ApiViewMetaClass(type):
    """MetaClass to incorporate the capacity of applying wrappers (decorators)
    without having to write it for every single method"""

    def __new__(self, name, bases, attrs):
        """if the class has a method post, get, put o delete, wrapp it with @handle_http_errors"""
        methods = ["post", "get", "put", "delete"]
        for metodo in methods:
            if metodo in attrs:
                attrs[metodo] = self.handle_http_errors(attrs[metodo])
        return super(ApiViewMetaClass, self).__new__(self, name, bases, attrs)

    @staticmethod
    def handle_http_errors(fn: Callable) -> Callable:
        """Wrapper that executes the code according to each request and deals with exceptions"""

        def new_fn(request, *args, **kwargs):
            response: ApiResponse
            try:
                try:
                    response = fn(request, *args, **kwargs)
                except Exception as e:
                    if len(e.args) != 2:
                        raise Exception(str(e), 500)
                    raise Exception(e)
            except Exception as e:
                response = ApiResponse(success="false", code=e.args[1], error=e.args[0])
            finally:
                return Response(json.dumps(dict(response)), status=response.code)

        return new_fn


class APIViewStructure(APIView, metaclass=ApiViewMetaClass):
    """Structure of the Views in this API"""

    not_available = Exception("endpoint_not_available", 405)

    def post(self) -> ApiResponse:
        raise self.not_available

    def get(self) -> ApiResponse:
        raise self.not_available

    def put(self) -> ApiResponse:
        raise self.not_available

    def delete(self) -> ApiResponse:
        raise self.not_available
