import json
import requests
from django.db.models import QuerySet
from rest_framework.response import Response
from app.apistructure import APIViewStructure
from app.models import Pokemon

class PokemonView(APIViewStructure):
    def post(self, request, *args, **kwargs):
        return Response(
            json.dumps({"message": "hi, endpoint is not available yet"}, status=200)
        )

    def get(self, request, *args, **kwargs):
        try:
            try:
                pokemon = kwargs["uid"]
                is_registered = Pokemon.is_stored_in_DB(pokemon)
                found: QuerySet[Pokemon]
                if is_registered:
                    found = Pokemon.objects.get(uid=pokemon)
                else:
                    # cluster_db = fetch cluster DB REST endpoint and ask for uid
                    # data: List[Dict['uid', 'data']] = cluster_db.data
                    # if not cluster_db.success:
                    #   fetched_data = fetch from pokeapi
                    #   data = [{'uid' = fetched_data.pokemonId, 'data' = fetched_data.pokemonData}]
                    #   post data to cluster_db endpoint
                    api_data = requests.get(
                        f"https://pokeapi.co/api/v2/pokemon/{pokemon}"
                    )
                    if not api_data:
                        raise Exception("Pokemon Not Found", 404)
                    data = json.loads(api_data.text)
                    data_to_store = {
                        "id": data["id"],
                        "name": data["name"],
                        "height": data["height"],
                        "weight": data["weight"],
                        "types": data["types"],
                        "stats": data["stats"],
                    }
                    Pokemon.objects.create(uid=data_to_store["id"], data=data_to_store)
                    found = Pokemon.objects.get(uid=pokemon)
                response = {
                    "success": "true",
                    "code": 200,
                    "data": found.get_object_data(),
                }
                return Response(json.dumps(response), status=200)
            except Exception as e:
                if len(e.args) != 2:
                    raise Exception(str(e), 500)
                raise Exception(e)
        except Exception as e:
            response = {
                "success": "false",
                "code": e.args[1],
                "error": e.args[0],
            }
            return Response(json.dumps(response), status=e.args[1])
